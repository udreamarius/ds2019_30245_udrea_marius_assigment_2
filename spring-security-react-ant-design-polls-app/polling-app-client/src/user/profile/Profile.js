import React, { Component } from 'react';
import { getUserProfile,getMessages } from '../../util/APIUtils';
import { Avatar, Tabs } from 'antd';
import { getAvatarColor } from '../../util/Colors';
import { formatDate } from '../../util/Helpers';
import LoadingIndicator  from '../../common/LoadingIndicator';
import './Profile.css';
import NotFound from '../../common/NotFound';
import ServerError from '../../common/ServerError';

const TabPane = Tabs.TabPane;

class Profile extends Component {
    constructor(props) {
        super(props);
       
        this.loadMessages=this.loadMessages.bind(this);
    }

    loadMessages(){
        getMessages().then(
            response=>{
                console.log(response);
            }
        )
    }
    componentDidMount() {
       
        this.loadMessages();
    }

    

    render() {

        return (
            <div className="profile">
                
            </div>
        );
    }
}

export default Profile;