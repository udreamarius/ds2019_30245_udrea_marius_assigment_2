package medicalcenter.controller;

import medicalcenter.model.Anomality;
import medicalcenter.model.PrescriptedMeds;
import medicalcenter.model.Prescription;
import medicalcenter.model.User;
import medicalcenter.payload.*;
import medicalcenter.repository.AnomalityRepository;
import medicalcenter.repository.UserRepository;
import medicalcenter.security.CurrentUser;
import medicalcenter.security.UserPrincipal;
import medicalcenter.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AnomalityRepository anomalityRepository;

    

    @GetMapping("/user/messages")
    @PreAuthorize("hasRole('CAREGIVER')")
    public ResponseEntity<List<String>> getMessages(@CurrentUser UserPrincipal currentUser){
        List<String> messages=new ArrayList<String>();
        User u=userService.findById(currentUser.getId());
        System.out.println(u.getPatiens().size());
        List<Anomality> l=anomalityRepository.findAll();
        for(Anomality a:l){
            if(u.getPatiens().contains(a.getUser())){
                messages.add("Pacientul cu numele "+a.getUser().getName()+ " a facut activitatea "+a.getActivity().replace('\t',' ')
                +"in perioada "+a.getStartDate()+" : "+a.getEndDate());
            }
        }
        return ResponseEntity.ok(messages);
    }


}



